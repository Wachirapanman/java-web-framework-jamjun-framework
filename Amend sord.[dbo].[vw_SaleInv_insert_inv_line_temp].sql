USE [SORD]
GO

/****** Object:  View [dbo].[vw_SaleInv_insert_inv_line_temp]    Script Date: 03/19/2015 16:38:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[vw_SaleInv_insert_inv_line_temp]
as

SELECT distinct line_grp.Inv_no, line_grp.Batch_No, line_grp.Batch_line_no, line_grp.Order_number
	, line_grp.order_lines_number, line_grp.po_lines_id, line_grp.po_number, line_grp.Apparel_styles_code
	, line_grp.StylesIntRefCode_fullcode, line_grp.pack_method, line_grp.Style_Pack_Ratio_ID
	, line_grp.Cartons_qty, line_grp.total_pairs_qty, line_grp.prices_CurrCode, line_grp.Currency_ex_rate
	, line_grp.size_trade_disc_percent, line_grp.size_trade_disc_value, line_grp.size_trade_disc_value_base
	, line_grp.TAX_Rate_Type_Code, line_grp.VAT_percent, line_grp.VAT_amount, line_grp.VAT_amount_base_curr
	--//WR update 19/03/2015 v.0.10.6-----------------
	-- solution for Invoice Amend – Amended VAT and Duplicate Invoice Lines displayed 
	--, siv_l.line_RoyaltyPercent, (line_grp.Inv_sales_value_base_curr*(siv_l.line_RoyaltyPercent/100)) as RoyaltyPrice_base
	, line_grp.RoyaltyPercent as line_RoyaltyPercent
	, line_grp.RoyaltyPrice_base as RoyaltyPrice_base
	--------------------------------------------------
	, siv_l.Discount_types, line_grp.sales_prices, line_grp.sales_prices_base_curr
	, (line_grp.Inv_sales_value-line_grp.discount_values+line_grp.VAT_amount) as line_inv_gross_vale
	, (line_grp.Inv_sales_value_base_curr-line_grp.discount_values_base_curr+line_grp.VAT_amount_base_curr) as line_inv_gross_value_base
	, line_grp.Inv_sales_value, line_grp.Inv_sales_value_base_curr, line_grp.FitsCode, line_grp.FitsLabel
	, line_grp.CartonSizes_ID, siv_l.CreatedDatetime_UTC, siv_l.CreatedBy, siv_l.Last_UpdatedDatetime_UTC
	, siv_l.Last_UpdatedBy, siv_l.ABMS_BATCH_ID, line_grp.despatch_number, line_grp.size_return_disc_percent
	, line_grp.size_return_disc_value, line_grp.size_return_disc_value_base
	, line_grp.size_profoma_disc_percent, line_grp.size_profoma_disc_value, line_grp.size_profoma_disc_value_base
	, siv_l.line_stlm_disc_percent1, (line_grp.Inv_sales_value-line_grp.discount_values+line_grp.VAT_amount)* (siv_l.line_stlm_disc_percent1/100) as line_stlm_disc_value1
	, (line_grp.Inv_sales_value_base_curr-line_grp.discount_values_base_curr+line_grp.VAT_amount_base_curr) * (siv_l.line_stlm_disc_percent1/100) as line_stlm_disc_value_base1
	, siv_l.line_stlm_disc_percent2, (line_grp.Inv_sales_value-line_grp.discount_values+line_grp.VAT_amount)* (siv_l.line_stlm_disc_percent1/100) as line_stlm_disc_value2
    --,siv_l.line_stlm_disc_value_base2 WR:02/02/2015:เอาออกเพราะมีข้อมูล ซ้ำกัน
	, 0 as line_stlm_disc_value_base2, line_grp.discount_values, line_grp.discount_values_base_curr
	, siv_l.Pairs_Adjust, siv_l.Sales_Value_Adjust, siv_l.Cost_Value_Adj
	, siv_l.Del_Inst_id, siv_l.line_Del_Inst_Value, siv_l.line_comment, siv_l.Del_Effective_Date
	, siv_l.line_Del_Inst_Vat_Amount, siv_l.line_Del_VAT_percent -- add new WR:16/02/2015
	--NH 20150318
	, siv_l.Inv_number_Ref, siv_l.Inv_line_number_Ref, siv_l.line_Del_Discount, siv_l.STD_Cost
	, siv_l.Sales_division_code, siv_l.BookGrpID, siv_l.IsStyleEffect, siv_l.LedgerAC

FROM SORD.dbo.vw_SaleInv_detail_temp_msg_group  as line_grp 
	left outer join SORD.dbo.SalesInv_line as siv_l on  line_grp.Inv_no = siv_l.Inv_No 
		and isnull(line_grp.despatch_number,'')=isnull(siv_l.despatch_number,'')
  


 














GO


