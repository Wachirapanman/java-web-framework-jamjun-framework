/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.common;

import jamjun.infra.annotation.validate.NotNullValidator;
import jamjun.infra.annotation.validate.NotNullValidator;
import jamjun.infra.exception.ErrorContainer;
import jamjun.infra.exception.ErrorModel;
import jamjun.infra.exception.JJValidateExeption;
import jamjun.infra.lang.JJString;
import jamjun.infra.model.FieldValidateModel;
import jamjun.infra.model.UserSession;
import jamjun.infra.resources.Alert;
import jamjun.infra.resources.ResourceManagment;
import java.lang.reflect.Field;
import java.util.AbstractList;
import java.util.ArrayList;

/**
 *
 * @author Java
 */
public class Validation {
    public static <T> T ValidateModel(T model) throws IllegalArgumentException, IllegalAccessException, InstantiationException, JJValidateExeption{
        boolean result = false;
        jamjun.infra.exception.ErrorContainer errorContainer = new ErrorContainer();
        errorContainer.errorList = new ArrayList<ErrorModel>();
        
        Class klazz = model.getClass();
        //ดู field ทั้งหมดที่ประกาศ
        for (Field f : klazz.getDeclaredFields()) {
            //<editor-fold desc="validate null object">
            boolean isNull = false;
            NotNullValidator notNullValidator = f.getAnnotation(NotNullValidator.class);
            if (notNullValidator != null){
                Object value = null;
                try{
                    value =f.get(model);
                }catch(Exception ex){
                    value = null;
                }
                //ถ้าเป็น string
                if(f.getType().getTypeName().equals(String.class.getTypeName())){
                    String strValue = (String) value;
                    if(JJString.IsNullOrEmty(strValue)){
                        isNull = true;
                    }
                }else{
                    if(value == null){
                        isNull = true;
                    }
                }
                if(isNull == true){
                    jamjun.infra.resources.Alert alertEnum = Alert.valueOf(notNullValidator.ResourceName());
                    String errorMsg =alertEnum.getString();
                     String tagMsg ="";
                    if(!JJString.IsNullOrEmty( notNullValidator.BaseResource())){
                        tagMsg = ResourceManagment.getMessage(notNullValidator.BaseResource(),notNullValidator.Tag());
                    }else{
                        tagMsg = notNullValidator.Tag();
                    }
                    
                    errorMsg = errorMsg + " ["+ tagMsg + "]";
                    System.out.println("anno_name:" + notNullValidator.ResourceName()+",field_name:"+f.getName() + ",field_value:"+errorMsg);
                    jamjun.infra.exception.ErrorModel errorModel = new ErrorModel();
                    errorModel.errorID = "valdate model";
                    errorModel.errorKey = "";
                    errorModel.errorMsg = errorMsg;
                    errorContainer.errorList.add(errorModel);
                }
            }
            //</editor-fold>
        }
        if(errorContainer.errorList.size() > 0){
            throw new jamjun.infra.exception.JJValidateExeption(errorContainer);
        }
        return model;
    }
    
    public static<T>  jamjun.infra.model.FieldValidateModel getFieldValidateModel(T model,String fieldName) throws Exception{
        try{
             jamjun.infra.model.FieldValidateModel result = new FieldValidateModel();
             Class klazz = model.getClass();
             Field filed;
             try{
                  filed = klazz.getField(fieldName);
             }catch(NoSuchFieldException | SecurityException e){
                 result= null;
                 return result;
             }
             result.filed = filed;
             NotNullValidator notNullValidator = null;
             try{
                notNullValidator = filed.getAnnotation(NotNullValidator.class);
             }catch(Exception e){
                 //not to do;
             }
             if (notNullValidator != null){
                 result.has_NotNullValidator = true;
             }
             return result;
        }catch(Exception ex){
            throw ex;
        }
    }
}
