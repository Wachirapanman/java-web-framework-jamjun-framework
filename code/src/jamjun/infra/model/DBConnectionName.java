/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.model;

/**
 *
 * @author Java
 */
public enum DBConnectionName {
    jsalon(1){
        @Override
        public String Name() {
            return "jsalon";
        }
    },
    jsalon2(2){
        @Override
        public String Name() {
            return "jsalon2";
        }
    };
    
    private int value;
    public abstract String Name();
    private DBConnectionName(int value) {
        this.value = value;
    }
}
