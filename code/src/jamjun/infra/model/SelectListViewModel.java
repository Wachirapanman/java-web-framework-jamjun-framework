/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Java
 */
public class SelectListViewModel {
    public SelectListViewModel()
    {
        listItem = new ArrayList<>();
    }
    public Object selected;
    /// <summary>
    /// autocomplete ลำดับการแสดงข้อมูล หรือไม่แสดงข้อมูล ex. Value,Text
    /// </summary>
    public String displayFormat;
    public List<SelectItemViewModel> listItem;
}
