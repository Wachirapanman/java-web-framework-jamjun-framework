/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.exception;

import java.util.ArrayList;

/**
 *
 * @author Java
 */
public class JJValidateExeption extends Exception {
    public ErrorContainer errorContainer;
    public JJValidateExeption(){
        errorContainer  = new ErrorContainer();
        errorContainer.errorList = new ArrayList<>();
    }
    public JJValidateExeption(ErrorContainer error){
        errorContainer = error;
    }
}
