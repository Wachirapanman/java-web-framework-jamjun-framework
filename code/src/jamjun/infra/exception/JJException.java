/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 *
 * @author Java
 */
public class JJException extends Exception {
    public ErrorContainer errorContainer;
    public JJException(){
        errorContainer  = new ErrorContainer();
        errorContainer.errorList = new ArrayList<>();
    }
    public JJException(ErrorContainer error){
        errorContainer = error;
    }
    public JJException(Object clazz,String errorKey, String errorMessage){
        String className = clazz.getClass().getName();
        errorMessage = className + " : " + errorMessage;
        ErrorModel errorModel = new ErrorModel();
        errorModel.errorKey =  errorKey;
        errorModel.errorMsg = errorMessage;
        errorContainer.errorList.add(errorModel);
    }
    public Exception SetException(Object clazz,Exception exp){
        
        try{
            String className = clazz.getClass().getName();
            String errorMsg = className +":"+ exp.toString();
            throw new Exception(errorMsg);
        }catch(Exception ex){
            return ex;
        }        
    }
    
    public String GetFullMessage(Exception e){
        try{
            String errorMsg = "";
            errorMsg = e.toString();
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            errorMsg = errorMsg + " StackTrace :" + errors.toString();
            return  errorMsg;
        }catch(Exception ex){
            throw ex;
        }
    }
}
