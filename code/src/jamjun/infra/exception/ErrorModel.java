/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.exception;

import java.util.Date;

/**
 *
 * @author Java
 */
public class ErrorModel {
    public String errorID;

    public String getErrorID() {
        return errorID;
    }

    public void setErrorID(String errorID) {
        this.errorID = errorID;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public void setErrorKey(String errorKey) {
        this.errorKey = errorKey;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getErrorTime() {
        return errorTime;
    }

    public void setErrorTime(Date errorTime) {
        this.errorTime = errorTime;
    }

    public String getErrorUserName() {
        return errorUserName;
    }

    public void setErrorUserName(String errorUserName) {
        this.errorUserName = errorUserName;
    }

    public String getErrorOnSystemName() {
        return errorOnSystemName;
    }

    public void setErrorOnSystemName(String errorOnSystemName) {
        this.errorOnSystemName = errorOnSystemName;
    }

    public String getErrorRefCode() {
        return errorRefCode;
    }

    public void setErrorRefCode(String errorRefCode) {
        this.errorRefCode = errorRefCode;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public String errorKey;
    public String errorMsg;
    public Date errorTime;
    public String errorUserName;
    public String errorOnSystemName;
    public String errorRefCode;
    
    /**
     *
     */
    public jamjun.infra.exception.ErrorType errorType;


}
