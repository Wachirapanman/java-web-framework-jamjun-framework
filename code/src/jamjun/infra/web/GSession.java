/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web;

import jamjun.infra.exception.ErrorContainer;
import jamjun.infra.exception.ErrorModel;
import jamjun.infra.model.UserSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Java
 */
public class GSession {
    public static HttpServletRequest Request;
    public static jamjun.infra.exception.ErrorContainer errorContainer;
    //public static ErrorRequest errorRequest;

    public static ErrorContainer getErrorContainer() {
        return errorContainer;
    }

    public static void setErrorContainer(ErrorContainer errorContainer) {
        GSession.errorContainer = errorContainer;
    }

    public static HttpServletRequest getRequest() throws Exception {
        try{
            if(Request == null){
                throw new Exception("Request is null");
            }else{
                return Request;
            }
        }catch(Exception e){
            throw e;
        }
    }

    public static void setRequest(HttpServletRequest Request) {
        GSession.Request = Request;
    }

    public static HttpServletResponse getResponse() {
        return Response;
    }

    public static void setResponse(HttpServletResponse Response) {
        GSession.Response = Response;
    }
    public static HttpServletResponse Response;
    
    public static UserSession setUserSession(UserSession userSession){
        HttpSession session = Request.getSession();
        session.setAttribute("userSession",userSession);
        return userSession;
    }
    public static UserSession getUserSession() throws Exception{
        try{
            UserSession userSession;
            HttpSession session = Request.getSession();
            userSession = (UserSession) session.getAttribute("userSession");
            if(userSession == null){
                throw new Exception("user Session is null");
            }else{
                return userSession;
            }
        }catch(Exception e){
            throw e;
        }
    }
    
    public static void setCookieSession(String cookieName,String sessionID){
        try{
            Cookie cookie = new Cookie(cookieName, sessionID);
            cookie.setMaxAge(24 * 60 * 60);// 24 hours.
            Response.addCookie(cookie);
        }catch(Exception e){
            throw e;
        }
    }
    
    public static String getResource(String key){
        try{
             HttpSession session = Request.getSession();
             ResourceBundle bundle = (ResourceBundle) session.getAttribute("ResourceBundle");
             return bundle.getString(key);
        }catch(Exception e){
            throw e;
        }
    }
    
    public static void setResource(){
        Locale locale = new Locale("th", "TH");
        //Locale locale1 = new Locale("en", "EN");
        Locale.setDefault(locale);
        ResourceBundle bundle =
        ResourceBundle.getBundle("resources.caption", locale);
        HttpSession session = Request.getSession();
        session.setAttribute("ResourceBundle",bundle);
    }
    
    public static void setLocale(){
        Locale locale = new Locale("th", "TH");
        //Locale locale = new Locale("en", "EN");
        Locale.setDefault(locale);
    }
    
}
