/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web;

/**
 *
 * @author Java
 */
public class Content extends Result{
    public Content(String strcontent){
        super.setResultType(ResultType.content);
        this.contentString =strcontent;
    }
    private String contentString;

    public String getContentString() {
        return contentString;
    }

    public void setContentString(String contentString) {
        this.contentString = contentString;
    }
}
