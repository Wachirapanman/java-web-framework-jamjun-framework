/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web;

/**
 *
 * @author Java
 */
public class View extends Result{
    public View(String view_name,Object model){
        this.view_name = view_name;
        this.model = model;
        super.setResultType(ResultType.view);
    }
            
    public String view_name;

    public String getView_name() {
        return view_name;
    }

    public void setView_name(String view_name) {
        this.view_name = view_name;
    }

    public Object getModel() {
        return model;
    }

    public void setModel(Object model) {
        this.model = model;
    }
    public Object model;
}
