/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web;

/**
 *
 * @author Java
 */
public enum ResultType { 
    view(1){
        @Override
        public String caption() {
            return "view";
        }
    },
    json(2){
        @Override
        public String caption() {
            return "json";
        }
    },file(3){
        @Override
        public String caption() {
            return "file";
        }
    },content(3){
        @Override
        public String caption() {
            return "content";
        }
    };
    private int value;
    public abstract String caption();
    private ResultType(int value) {
        this.value = value;
    }
}

