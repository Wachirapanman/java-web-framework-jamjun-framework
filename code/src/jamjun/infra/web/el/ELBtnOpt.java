/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web.el;

import jamjun.infra.web.el.ELBtnIcon;

/**
 *
 * @author Java
 */
public class ELBtnOpt extends ELOpt {
    public String caption ="";
    public Boolean disable = false;
    public ELBtnIcon btnIcon = ELBtnIcon.none;
    public String onClick="";
    public ELColorBtn color = ELColorBtn.none;
    public ELBtnSize size = ELBtnSize.px32;
    public boolean visible = true;
    public jamjun.infra.model.ActionType actionType = null;
}
