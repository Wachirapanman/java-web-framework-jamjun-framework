/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web.el;

/**
 *
 * @author Java
 */
public enum ELBtnSize {
    px26(1){
        @Override
        public String strValue(){
            return "26";
        }
        @Override
        public String icon(){
            return "26";
        }
        
    },
    px32(2){
        @Override
        public String strValue(){
            return "32";
        }
        @Override
        public String icon(){
            return "32";
        }
        
    },
    px64(3){
        @Override
        public String strValue(){
            return "64";
        }
        @Override
        public String icon(){
            return "64";
        }
    }
    ;
    
    private int value;
    public abstract String strValue();
    public abstract String icon();
    private ELBtnSize(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
    
    public static ELBtnSize fromValue(int value) {  
        for (ELBtnSize my: ELBtnSize.values()) {  
            if (my.value == value) {  
                return my;  
            }  
        }  
        return null;  
    }  
}
