/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web.el;


/**
 *
 * @author Java
 */
public class ELOpt {
        public String value ="";
        public String idAttr="";
        public String nameAttr="" ;
        public String classAttr="";
        public String styleAttr="";
        public String otherAttr="";
        public String title="";
        public Integer tabindex;
        public Boolean disable=false;
        public Boolean isValidateEmpty= false;
        public Boolean isLabel = false;
        public String  placeholder ="";
        public Boolean isReadOnly = false;
        public Object model = null;
        public String fieldName ="";
        public Boolean canEdit= false;

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }
    
    public void setValidate(Object _model,String _fieldName){
        model = _model;
        fieldName = _fieldName;
    }
        
    public String strTabIndex(){
        String result = "";
        result = (tabindex == null) ? "" : " tabindex='" + tabindex + "' ";
        return result;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIdAttr() {
        return idAttr;
    }

    public void setIdAttr(String idAttr) {
        this.idAttr = idAttr;
    }

    public String getNameAttr() {
        return nameAttr;
    }

    public void setNameAttr(String nameAttr) {
        this.nameAttr = nameAttr;
    }

    public String getClassAttr() {
        return classAttr;
    }

    public void setClassAttr(String classAttr) {
        this.classAttr = classAttr;
    }

    public String getStyleAttr() {
        return styleAttr;
    }

    public void setStyleAttr(String styleAttr) {
        this.styleAttr = styleAttr;
    }

    public String getOtherAttr() {
        return otherAttr;
    }

    public void setOtherAttr(String otherAttr) {
        this.otherAttr = otherAttr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
        
}
