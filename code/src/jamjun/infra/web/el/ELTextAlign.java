/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web.el;

/**
 *
 * @author Java
 */
public enum ELTextAlign {
//    
//    [StringValue("none")]
//            none = 0,
//            [StringValue("textAlign_left")]
//            left = 1,
//            [StringValue("textAlign_center")]
//            center = 2,
//            [StringValue("textAlign_right")]
//            right = 3,

    none(1){
        @Override
        public String caption() {
            return "none";
        }
        @Override
        public String strValue() {
            return "none";
        }
    },
    left(2){
        @Override
        public String caption() {
            return "left";
        }
        @Override
        public String strValue() {
            return "textAlign_left";
        }
    },
    right(3){
        @Override
        public String caption() {
            return "right";
        }
        @Override
        public String strValue() {
            return "textAlign_right";
        }
    },
    center(4){
        @Override
        public String caption() {
            return "center";
        }
        @Override
        public String strValue() {
            return "textAlign_center";
        }
    };
    
    private int value;
    public abstract String caption();
    public abstract String strValue();
    
    private ELTextAlign(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
    
    public static ELTextAlign fromValue(int value) {  
        for (ELTextAlign my: ELTextAlign.values()) {  
            if (my.value == value) {  
                return my;  
            }  
        }  
        return null;  
    }  
}
