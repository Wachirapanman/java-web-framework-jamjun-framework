/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web.el;

/**
 *
 * @author Java
 */
public enum ELColorBtn {
    none(1){
        @Override
        public String strValue(){
            return "";
        }
    },
    red(2){
        @Override
        public String strValue(){
            return "btn-color-red";
        }
    }
    ,green(3){
        @Override
        public String strValue(){
            return "btn-color-green";
        }
    };
    
    private int value;
    public abstract String strValue();
    private ELColorBtn(int value) {
        this.value = value;
    }
    public int getVal(){
        return this.value;
    }
}
