/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.web;

/**
 *
 * @author Java
 */
public class Json extends Result{
    public Json(String strJson)
    {
        super.setResultType(ResultType.json);
        this.strJson = strJson;
    }
    public Json(Object obj)
    {
        super.setResultType(ResultType.json);
        this.obj = obj;
    }
    private Object obj;

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
    private String strJson;

    public String getStrJson() {
        return strJson;
    }

    public void setStrJson(String strJson) {
        this.strJson = strJson;
    }
    
}
