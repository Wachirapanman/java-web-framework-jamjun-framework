/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jamjun.infra.lang;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
 *
 * @author Java
 */
public class JJDate {
    public static Locale defalutLocal;
    public static void setLocal()
    {
        defalutLocal = Locale.getDefault();
        Locale locale = new Locale("en", "EN");
        Locale.setDefault(locale);
    }
    public static void restoreLocal(){
        Locale.setDefault(JJDate.defalutLocal);
    }
    public static String ConvertToStr(Date date){
        String strDate = "";
        try{
            if(date == null){
                return strDate;
            }
            JJDate.setLocal();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            strDate=sdf.format(date);
            return strDate;
        }catch(Exception e){
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    public static String ConvertToStrDateTime(Date date){
        String strDate = "";
        try{
            if(date == null){
                return strDate;
            }
            JJDate.setLocal();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            strDate=sdf.format(date);
            return strDate;
        }catch(Exception e){
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    
    public static String ConvertToStr(Date date,TimeZone timeZone){
        String strDate = "";
        try{
            if(date == null){
                return strDate;
            }
            JJDate.setLocal();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendar.setTimeZone(timeZone);
            Date dateNew =  calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy ");
            strDate=sdf.format(dateNew);
            return strDate;
        }catch(Exception e){
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static String ConvertToStrDateTime(Date date,TimeZone timeZone){
        String strDate = "";
        JJDate.setLocal();
        try{
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendar.setTimeZone(timeZone);
            Date dateNew =  calendar.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            strDate=sdf.format(dateNew);
            return strDate;
        }catch(Exception e){
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    
    public static Date StrConverToDateUTC(String strdate) throws Exception
    {
        JJDate.setLocal();
        Date result= new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        try {
            result = sdf.parse(strdate);
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (ParseException e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }

    public static Date StrConverToDateTimeUTC(String strdatetime) throws Exception
    {
        JJDate.setLocal();
        Date result= new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        try {
            result = sdf.parse(strdatetime);
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (ParseException e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static Date StrConverToDate(String strdate) throws Exception
    {
        JJDate.setLocal();
        Date result= new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            result = sdf.parse(strdate);
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (ParseException e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    
    
    public static Date StrConverToDateTime(String strdate) throws Exception
    {
        JJDate.setLocal();
        Date result= new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            result = sdf.parse(strdate);
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (ParseException e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    /*
    ใช้ save ลง db
    */
    public static Date DateConverToDateFormateUS(Date date1) throws Exception
    {
        String strdate = "";
        Date result= new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            strdate = sdf.format(date1);
            result = sdf.parse(strdate);
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (Exception e) {
            throw e;
        }finally{
            
        }
    }
    /*
    ใช้ save ลง db
    */
    public static Date DateTimeConverToDateTimeFormateUS(Date date1) throws Exception
    {
        String strdate = "";
        Date result= new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            strdate = sdf.format(date1);
            result = sdf.parse(strdate);
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (Exception e) {
            throw e;
        }finally{
            
        }
    }
    
    
    
    public static String GetYear(Date date) throws Exception{
        JJDate.setLocal();
        try{
            String result;
            Locale locale = new Locale("en", "EN");
            Locale.setDefault(locale);
            final DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
            final Calendar c = Calendar.getInstance();
            String fmDate = sdf.format(date);
            c.setTime(sdf.parse(fmDate));
            int year = c.get(Calendar.YEAR);
            result = String.valueOf(year);
            Locale.setDefault(defalutLocal);
            return result;
        }catch(ParseException ex){
            Locale.setDefault(defalutLocal);
            throw ex;
        }finally{
            JJDate.restoreLocal();
        }
    }
    public static int GetYearToInt(Date date) throws Exception{
        try{
            int result;
            String strResult = GetYear(date);
            result = Integer.parseInt(strResult);
            return result;
        }catch(ParseException ex){
            throw ex;
        }finally{
        }
    }
    
    public static String GetDay(Date date) throws Exception{
        JJDate.setLocal();
        try{
            String result;
            final DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
            final Calendar c = Calendar.getInstance();
            String fmDate = sdf.format(date);
            c.setTime(sdf.parse(fmDate));
            int year = c.get(Calendar.DAY_OF_MONTH);
            result = String.valueOf(year);
            return result;
        }catch(ParseException ex){
            throw ex;
        }finally{
            JJDate.restoreLocal();
        }
    }
    public static int GetDayToInt(Date date) throws Exception{
        try{
            int result;
            String strResult = GetDay(date);
            result = Integer.parseInt(strResult);
            return result;
        }catch(ParseException ex){
            throw ex;
        }finally{
        }
    }
    
    public static String GetMonth(Date date) throws Exception{
        JJDate.setLocal();
        try{
            String result;
            final DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
            final Calendar c = Calendar.getInstance();
            String fmDate = sdf.format(date);
            c.setTime(sdf.parse(fmDate));
            int year = c.get(Calendar.MONTH) + 1;
            result = String.format("%02d",year);
            return result;
        }catch(ParseException ex){
            throw ex;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static int GetMonthToInt(Date date) throws Exception{
        try{
            int result;
            String strResult = GetMonth(date);
            result = Integer.parseInt(strResult);
            return result;
        }catch(ParseException ex){
            throw ex;
        }finally{
        }
    }
    
    public static Date Now(){
        JJDate.setLocal();
        try {
            Date result= new Date();
            return result;
            //String newDateString = sdf.format(startDate);
        } catch (Exception e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static Date AddYear(Date date,int numberOfyear) throws Exception{
        JJDate.setLocal();
        try {
            Calendar c = Calendar.getInstance(); 
            c.setTime(date); 
            c.add(Calendar.YEAR,numberOfyear);
            date = c.getTime();
            return date;
        } catch (Exception e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static Date AddDate(Date date,int numberOfdate) throws Exception{
        JJDate.setLocal();
        try {
            Calendar c = Calendar.getInstance(); 
            c.setTime(date); 
            c.add(Calendar.DATE,numberOfdate);
            date = c.getTime();
            return date;
        } catch (Exception e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static Date AddMONTH(Date date,int numberOfMONTH) throws Exception{
        JJDate.setLocal();
        try {
            Calendar c = Calendar.getInstance(); 
            c.setTime(date); 
            c.add(Calendar.MONTH,numberOfMONTH);
            date = c.getTime();
            return date;
        } catch (Exception e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
    
    public static Date AddWEDNESDAY(Date date,int numberOfWEDNESDAY) throws Exception{
        JJDate.setLocal();
        try {
            Calendar c = Calendar.getInstance(); 
            c.setTime(date); 
            c.add(Calendar.WEDNESDAY,numberOfWEDNESDAY);
            date = c.getTime();
            return date;
        } catch (Exception e) {
            throw e;
        }finally{
            JJDate.restoreLocal();
        }
    }
}
